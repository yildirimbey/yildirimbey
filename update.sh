#!/bin/bash

#Name     : Thunderbolt Shell Auto Updater
#Author   : YıLDıRıMBeY
#Mail     : yildirimbey@outlook.com
#USAGE    : bash update.sh

ver=4.5
clear
echo "==================================================="
echo "    Thunderbolt Shell Auto Root Updater v$ver"
echo "                 by YıLDıRıMBeY"
echo "==================================================="
echo "updating script.."
wget -q --no-check-certificate https://raw.githubusercontent.com/nilotpalbiswas/Auto-Root-Exploit/master/autoroot.sh -O $0
sleep 2
bash $0
function s0 {
ck=$(wget https://raw.githubusercontent.com/nilotpalbiswas/Auto-Root-Exploit/master/autoroot.sh --timeout=5 -q -O - | grep "ver=" | cut -d"=" -f2)
mtach=$(echo $ck | cut -d" " -f1)
if [ "$mtach" != "$ver" ]; then
otp="0   | *Bir güncelleme mevcut"
else
otp="    | Güncelleme mevcut değil"
fi
}
ping -c 1 raw.githubusercontent.com  -i 1000 > /dev/null
  [ $? -eq 0 ] && s0
  [ $? -eq 1 ] && otp="*..Güncelleştirmeleri denetlerken hata oluştu..*"

echo -e "
	Usage:
	┌──[$USER@`hostname`:~]
	└──╼ bash ""$0 {2|3|4|bsd|app|all|0}

           No. |     Commands list 
          ------------------------------------
           2   | for kernel version 2.6 all
           3   | for kernel version 3 all
           4   | for kernel version 4 all
           bsd | for freebsd & openbsd all
           app | for apple macos all
           all | for kernel 2.6,3,4 bsd & app all
           $otp
" 